from datetime import datetime
import argparse as ap
import configparser as cp
from synclib import jss


def parse_myargs():
    # Build the argparser and parse the args
    parser = ap.ArgumentParser()
    parser.add_argument("type", help="3sys Query Type", choices=["FormCode", "SubjectSet"])
    parser.add_argument("term", help="3sys Query Term")
    parser.add_argument("group", help="JSS Group Name")
    parser.add_argument("--dry-run", help="Run Queries, but do not sync", default=False, action="store_true")
    parser.add_argument("--config", help="Specify a config filepath", default="/etc/sync.config")
    args = parser.parse_args()
    return args


def parse_config(file: str):
    # Build the config parser and parse the config
    config = cp.ConfigParser()
    try:
        config.read(file)
    except OSError:
        print("No config file found")
    return config


def print_summary(qtype: str, qterm: str, group: str, dry_run: bool):
    # print a summary of the sync parameters
    print("\n__________3SYS-JSS-SYNC__________")
    print("Date: {}".format(str(datetime.now().strftime("%Y-%m-%d %H:%M"))))
    print("Query Type: {}".format(qtype))
    print("Query Term: {}".format(qterm))
    print("JSS Group: {}".format(group))
    if dry_run is False:
        print("\n_____________SYNCING_____________")
    else:
        print("\n_____________DRY_RUN_____________")


def print_results(adds: jss.SyncSubmission, dels: jss.SyncSubmission):
    adds_suc = len(adds.succeeded)
    adds_rej = adds.rejected
    dels_suc = len(dels.succeeded)
    dels_rej = dels.rejected
    print("\n_____________RESULTS_____________")
    print("Number of users added: {}".format(adds_suc))
    if len(adds_rej) != 0:
        print("Failed to add:", *adds_rej, sep=" ")
    print("Number of users removed: {}".format(dels_suc))
    if len(dels_rej) != 0:
        print("Failed to remove:", *dels_rej, sep=" ")


def calc_sync_time(start_time: datetime):
    end_time = datetime.now()
    diff_time = end_time - start_time
    return diff_time.seconds


def calculate_diffs(users: list, members: list ):
    adds = jss.SyncSubmission(set([u.username for u in users if u.username not in members and u.username is not None]),
                              "user_additions")
    dels = jss.SyncSubmission(set([m for m in members if m not in [u.username for u in users]]),
                              "user_deletions")
    omits = set([u.pid for u in users if u.username is ""])
    return {"adds": adds, "dels": dels, "omits": omits}


