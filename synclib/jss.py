import xml.etree.ElementTree as ET
import requests
from requests.auth import HTTPBasicAuth as BasicAuth
from synclib.server import Server


def _create_xml(username: str, tag: str):
    user_group_root = ET.Element('user_group', )
    users_tag = ET.SubElement(user_group_root, tag)
    user_tag = ET.SubElement(users_tag, 'user')
    username_tag = ET.SubElement(user_tag, 'username')
    username_tag.text = username
    xml = ET.tostring(user_group_root)
    return xml


def query_group(server: Server, jss_group: str):
    query_url = '{}/JSSResource/usergroups/name/{}'.format(server.url, jss_group)
    r = requests.get(query_url, auth=BasicAuth(server.username, server.password))
    if r.status_code == 200:
        return set([e.text for e in ET.fromstring(r.text.lower()).findall(".//username")])
    else:
        return []


class SyncSubmission:
    def __init__(self, users: set, action: str):
        self.usernames = users
        self.action = action
        self.succeeded = []
        self.rejected = []

    def post(self, server: Server, jss_group: str):
        query_url = '{}/JSSResource/usergroups/name/{}'.format(server.url, jss_group)
        for u in self.usernames:
            if u != "":
                data = _create_xml(u, self.action)
                r = requests.put(query_url, data, auth=BasicAuth(server.username, server.password))
                if r.status_code == 201:
                    self.succeeded.append(u)
                else:
                    self.rejected.append(u)
