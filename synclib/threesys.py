from datetime import date
from requests import get
from requests.auth import HTTPBasicAuth as BasicAuth
from synclib.server import Server


def parse_response(string: str):
    intab = "{}[]:\"\n"
    trantab = str.maketrans(intab, " " * len(intab))
    return string.lower().translate(trantab).split()


def query_users(server: Server, query_type: str, query_term: str):
    query = None
    acad_yr = str((lambda m, y: y - 1 if m <= 6 else y)(date.today().month, date.today().year))
    if query_type.lower() == "formcode":
        query = server.url + "/CurrentPupil?$filter=(AcademicYearCode eq {} and startswith(FormCode, '{}'))" \
                     "&$select=ID".format(acad_yr, query_term)
    elif query_type.lower() == "subjectset":
        query = server.url + "/SubjectSet?$filter=(AcademicYearCode eq {} and startswith(SubjectSetCode, '{}'))&" \
                     "$select=Pupils&$expand=Pupils($select=PupilID)".format(acad_yr, query_term)
    r = get(query, auth=BasicAuth(server.username, server.password))
    if r.status_code == 200:
        return set([User(s) for s in parse_response(r.text) if s.isdigit()])
    else:
        return []


class User:
    def __init__(self, pid: str):
        self.pid = pid
        self.username = ""

    def get_username(self, server: Server):
        q_url = server.url + "/CurrentPupil({})?$select=PupilPerson&$expand=PupilPerson($select=EmailAddresses;" \
                      "$expand=EmailAddresses($select=EmailAddress))".format(str(self.pid))
        r = get(q_url, auth=BasicAuth(server.username, server.password))
        if r.status_code == 200:
            for s in parse_response(r.text):
                if s.find("@") != -1:
                    self.username = s.split("@")[0]
                    break
